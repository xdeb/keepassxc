## keepassxc

[KeePassXC](https://www.keepassxc.org/) packaging, which products the latest version in .deb format. Since this is the latest version, you are running on the cutting edge if you choose to install.
